Normes:

Nom:

    -Avoir le nom complet de l'association:
        Exemple: GCI > Green Cross International

Mail:

    - Tout écrire en minuscule
        Exemple: GCInterNational@gci.ch > gcinternational@gci.ch

Type:
    
    - Annimalier
    - Environement
    - Humanitaire
    - Ecologique
    - Renouvelable

Langue:

    - Ecrire la langue entière sans abréviations
        Exemple: France > Français
                 Etats-Unis > Anglais
                 Russie > Russe
                 
Merci de bien tout respecter lors de l'ajout d'une association.